package com.example.fragmentsbasic;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.example.fragmentsbasic.Fragments.Tested;

public class MainActivity extends AppCompatActivity {
    public static FragmentManager fragmentManager;
    FragmentTransaction transaction;
    FrameLayout frameLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        frameLayout = findViewById(R.id.frameLayout);
        fragmentManager = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();

        if (frameLayout!=null)
        {
            if (savedInstanceState == null)
            {
               transaction.add(R.id.frameLayout,new Tested(),null).commit();

            }
        }
    }
}
