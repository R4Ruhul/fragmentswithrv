package com.example.fragmentsbasic;

public class Datamodel {
    public String Name;
    public String District;

    public Datamodel(String name, String district) {
        Name = name;
        District = district;
    }

    public Datamodel() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDistrict() {
        return District;
    }

    public void setDistrict(String district) {
        District = district;
    }
}
