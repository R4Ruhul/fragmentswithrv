package com.example.fragmentsbasic.Fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.fragmentsbasic.MainActivity;
import com.example.fragmentsbasic.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class Tested extends Fragment {

    public Tested() {
        // Required empty public constructor
    }
    Button button;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tested, container, false);

        button = view.findViewById(R.id.test);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.fragmentManager.beginTransaction().replace(R.id.frameLayout,new TriedFragment(),null).addToBackStack(null).commit();
            }
        });

        return view;
    }
}
